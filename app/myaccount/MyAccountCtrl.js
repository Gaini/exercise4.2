'use strict';

angular
    .module('myApp')
    .service('usersService', function () {
        var data = [
        ];

        return {
            users() {
                return data;
            },
            createUser: function(userData) {
                var currentIndex = data.length + 1;
                data.push({
                    id:currentIndex,
                    name:userData.name,
                    email:userData.email,
                    phone:userData.phone
                });
                return data;
            },
        };
    })
    .controller('MyAccountCtrl', function ($scope, usersService) {

        $scope.getUsers = usersService.users();

        $scope.createUser = function (userData) {
            usersService.createUser(userData);
        };
    });