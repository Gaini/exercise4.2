'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ui.router',
    'ngMessages'
]).config(function ($stateProvider) {

    $stateProvider
        .state({
            name: 'page1',
            url: '/page1',
            templateUrl: 'page1/page1.html',
            controller: function () {
            }
        })
        .state({
            name: 'page2',
            url: '/page2',
            templateUrl: 'page2/page2.html',
            controller: function () {
            }
        })
        .state({
            name: 'page3',
            url: '/page3',
            templateUrl: 'page3/page3.html',
            controller: function () {
            }
        })
        .state({
            name: 'myaccount',
            url: '/myaccount',
            templateUrl: 'myaccount/myaccount.html',
            controller: 'MyAccountCtrl as vm'
        });
})